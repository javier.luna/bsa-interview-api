import { Sequelize } from 'sequelize';
import { env } from '../env';

// Option 1: Passing parameters separately
const sequelize = new Sequelize(env.mysql.database, env.mysql.username, env.mysql.password, {
  host: env.mysql.host,
  dialect: 'mysql',
});

export { sequelize };
