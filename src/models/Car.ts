import { DataType, Model } from 'sequelize-typescript';
import { sequelize } from '../config/db';

export interface CarObject {
  make?: string,
  model?: string,
  year?: number,
  color?: string,
  vin?: string,
}

export class Car extends Model {}
Car.init({
  // attributes
  make: {
    type: DataType.STRING,
    allowNull: false,
  },
  model: {
    type: DataType.STRING,
    allowNull: false,
  },
  year: {
    type: DataType.NUMBER,
    allowNull: false,
  },
  color: {
    type: DataType.STRING,
    allowNull: false,
  },
  vin: {
    type: DataType.STRING,
    allowNull: false,
  },
}, {
  sequelize,
  modelName: 'car',
  // options
  freezeTableName: true,
  timestamps: false,
});
