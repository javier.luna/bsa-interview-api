import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import { describe, it, before } from 'mocha';
import server from '../index';
import { sequelize } from '../src/config/db';

chai.use(chaiHttp);

describe('API Test', () => {
  before(() => {
    setTimeout(() => {
      sequelize
        .authenticate()
        .then(() => {
          console.log('Connection has been established successfully.');
        })
        .catch((err) => {
          console.error('Unable to connect to the database:', err);
        });
    }, 2000);
  });
  describe('Get all Cars', () => {
    it('should get all cars', (done) => {
      chai.request(server)
        .get('/car')
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
  describe('Get some Cars', () => {
    it('should get all cars', (done) => {
      chai.request(server)
        .get('/car')
        .query({ make: 'Audi' })
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
  describe('Create a valid Car', () => {
    it('should create a car', (done) => {
      chai.request(server)
        .post('/car')
        .send({
          make: 'Test',
          model: 'Test',
          year: 2020,
          color: 'Test',
          vin: 'Test',
        })
        .end((err, res) => {
          expect(res).to.have.status(201);
          done();
        });
    });
  });
  describe('Try to create an invalid Car', () => {
    it('should not allow to create a car if vin already exist', (done) => {
      chai.request(server)
        .post('/car')
        .send({
          make: 'Test',
          model: 'Test',
          year: 2020,
          color: 'Test',
          vin: 'Test',
        })
        .end((err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });
  });
  describe('Update a Car', () => {
    it('should update a car given a valid id', (done) => {
      chai.request(server)
        .patch('/car/1')
        .send({
          make: 'Testing',
        })
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
  describe('Update a car invalid id', () => {
    it('should not update a car given an invalid id', (done) => {
      chai.request(server)
        .patch('/car/10010')
        .send({
          make: 'Testing',
        })
        .end((err, res) => {
          expect(res).to.have.status(500);
          done();
        });
    });
  });
  describe('Delete a car', () => {
    it('should delete a car', (done) => {
      chai.request(server)
        .delete('/car/1')
        .end((err, res) => {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
});
