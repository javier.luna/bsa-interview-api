import { WhereOptions } from 'sequelize';
import { Car, CarObject } from '../models/index';

export class CarRepository {
  public async find(filters?: WhereOptions) {
    if (!filters) return Car.findAll();
    return Car.findAll({ where: filters });
  }

  public async findById(id: number) {
    return Car.findOne({ where: { id } });
  }

  public async findByVin(vin: string) {
    return Car.findOne({ where: { vin } });
  }

  public async create(data: Car) {
    return Car.create(data);
  }

  public async update(id: number, data: CarObject) {
    return Car.update(data, { where: { id } });
  }

  public async delete(id: number) {
    return Car.destroy({ where: { id } });
  }
}
