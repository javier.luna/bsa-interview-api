# bsa-interview-api

This is my solution for the interview exercise. This was developed on ubuntu 16.04

## Requirements

- Mysql (Ver 14.14 Distrib 5.7.29)
- npm (v 6.13.4)
- node (v 12.15.0)
- mocha installed globally ``` npm install mocha -g ```

### Step 1: Set up the environment

1. Copy the .env.example into a new file .env on root (verify that your credentials are right for your mysql user)

2. Run the following command:

```
npm run setup
```

### Step 2: Run the tests

Run the following command:

```
npm run test
```
