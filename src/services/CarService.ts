import { WhereOptions } from 'sequelize';
import { CarRepository } from '../repositories/index';
import { CarObject } from '../models';

const carRepository = new CarRepository();

export class CarService {
  public find(filters?: WhereOptions) {
    return carRepository.find(filters);
  }

  public async create(data) {
    const { vin } = data;
    // Validate if the vin already exist
    const car = await carRepository.findByVin(vin);
    if (car) {
      throw new Error('Car already exist');
    }
    return carRepository.create(data);
  }

  public async update(id: number, data: CarObject) {
    const carInfo = { ...data };
    const response = [];
    const car = await carRepository.findById(id);
    if (!car) {
      throw new Error('No car for given id');
    }
    const { vin } = carInfo;
    if (vin) {
      const carWithVin = await carRepository.findByVin(vin);
      if (carWithVin) {
        delete carInfo.vin;
        response.push({ message: 'Vin already exist on a car registry' });
      }
    }
    delete car.id;
    const newCar = Object.assign(car.toJSON(), carInfo) as CarObject;
    await carRepository.update(id, newCar);
    return response;
  }

  public async delete(id: number) {
    const car = await carRepository.findById(id);
    if (!car) {
      throw new Error('No car for given id');
    }
    await carRepository.delete(id);
  }
}
