import * as dotenv from 'dotenv';
import * as path from 'path';

dotenv.config({ path: path.join(process.cwd(), `.env${((process.env.NODE_ENV === 'test') ? '.test' : '')}`) });

function getFromEnv(key: string): string {
  if (typeof process.env[key] === 'undefined') {
    throw new Error(`Environment variable ${key} is not set.`);
  }

  return process.env[key] as string;
}
export const env = {
  general: {
    port: getFromEnv('PORT'),
    url: getFromEnv('URL'),
  },
  mysql: {
    database: getFromEnv('MYSQL_DATABASE'),
    username: getFromEnv('MYSQL_USERNAME'),
    password: getFromEnv('MYSQL_PASSWORD'),
    host: getFromEnv('MYSQL_HOST'),
    tableCar: getFromEnv('MYSQL_TABLE_CAR'),
  },
};
