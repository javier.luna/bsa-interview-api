import express from 'express';
import { CarService } from '../services';
// import { sequelize } from '../config/db';

const CarResources = express.Router();
const carService = new CarService();

CarResources.get('/', async (req, res) => {
  console.log('Getting the data');
  try {
    const data = await carService.find(req.query);
    return res.status(200).json(data);
  } catch (e) {
    console.info(e);
    return res.status(500).json({ message: e.message });
  }
});

CarResources.post('/', async (req, res) => {
  console.log('Creating new registry');
  try {
    const data = await carService.create(req.body);
    return res.status(201).json(data);
  } catch (e) {
    console.info(e);
    return res.status(500).json({ message: e.message });
  }
});

CarResources.patch('/:id', async (req, res) => {
  console.log('Updating registry');
  try {
    const { id } = req.params;
    const response = await carService.update(parseInt(id, 10), req.body);
    return res.status(200).json({ message: 'Data updated', warnings: response });
  } catch (e) {
    console.info(e);
    return res.status(500).json({ message: e.message });
  }
});

CarResources.delete('/:id', async (req, res) => {
  console.log('Deleting registry');
  try {
    const { id } = req.params;
    await carService.delete(parseInt(id, 10));
    return res.status(200).json({ message: 'Data deleted' });
  } catch (e) {
    console.info(e);
    return res.status(500).json({ message: e.message });
  }
});

export { CarResources };
