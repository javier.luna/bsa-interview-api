import express from 'express';
import { CarResources } from '../resources';

const router = express.Router();

router.use('/car', CarResources);

export { router };
