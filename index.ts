import express from 'express';
import bodyParser from 'body-parser';
import { router, env } from './src';
import { sequelize } from './src/config/db';

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(router);

const server = app.listen(env.general.port, () => {
  console.log(`Example app listening on port ${env.general.port}!`);
});

export default server;
